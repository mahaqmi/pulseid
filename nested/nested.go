package nested

import (
	"errors"
	"github.com/labstack/echo/v4"
	"html/template"
	"io"
	"time"
)

// TemplateRegistry - Define the template registry struct
type TemplateRegistry struct {
	Templates map[string]*template.Template
}

// To implement e.Renderer interface later
func (t *TemplateRegistry) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	tmpl, ok := t.Templates[name]
	if !ok {
		err := errors.New("Template not found -> " + name)
		return err
	}
	return tmpl.ExecuteTemplate(w, "index.html", data)
}

func HumanDate(t time.Time) string {
	return t.Format("2 Jan 2006 at 15:04")
}
