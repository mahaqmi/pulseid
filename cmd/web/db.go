package main

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
)

func DBConnect() *sqlx.DB {
	// Declare all the necessary params for connecting to DB
	var (
		host, user, password, dbname string
		port                         int
	)

	// droplet server IP; port; username that access; password; database name
	host = "demo.cmxjfvvv2qs9.ap-southeast-1.rds.amazonaws.com"
	port = 5432
	user = "postgres"
	password = "qwerty123"
	fmt.Printf("%s %s %s %d", host, user, password, port)

	// DB information for the Task depending on whether its for production or localhost testing
	if Dev == true {
		dbname = "demo3"
	} else if Dev == false {
		//Other DB during production
		dbname = "demo3"
	}
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sqlx.Connect("postgres", psqlInfo)
	if err != nil {
		log.Fatalln(err)
	}
	// This is needed as sqlx uses strings.ToLower on Go names to map to names coming from the result of rows.Columns()
	//https://github.com/jmoiron/sqlx/issues/234
	db.MapperFunc(func(s string) string { return s })

	//db, err := sqlx.Connect("postgres", "host=db user=pg-user password=password dbname=sample_db sslmode=disable")
	//if err != nil {
	//	log.Fatalln(err)
	//}
	return db
}
