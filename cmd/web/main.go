package main

import (
	"flag"
	"fmt"
	"github.com/mholt/certmagic"
)

var Dev bool
var Port string

func main() {
	dev := flag.Bool("dev", true, "change dev mode based on env run")
	port := flag.String("port", ":80", "port")
	flag.Parse() //Parse all flag from command line here

	Dev = *dev
	Port = *port

	r := New() // run echo instance

	// If in development env -> start http instance at `Port`; if production -> run HTTPS with proper domain
	if Dev == true {
		r.Logger.Fatal(r.Start(Port)) // Run the echo instance at `Port`

	} else {

		// For HTTPS. Echo have problem, it uses v1 api
		err := certmagic.HTTPS([]string{"www.mohdaqib.me", "mohdaqib.me"}, r)
		if err != nil {
			fmt.Println(err)
		}
	}
}
