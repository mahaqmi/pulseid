package main

import (
	"dev/PulseID/handler"
	"dev/PulseID/nested"
	"dev/PulseID/pkg/postgres"
	"dev/PulseID/utils"
	"github.com/gorilla/sessions"
	"github.com/labstack/echo-contrib/prometheus"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"html/template"
	"net/http"
)

func New() *echo.Echo {
	d := DBConnect() // Engage connection with DB
	h := handler.NewHandler(postgres.NewInvitationModel(d))

	// Server
	e := echo.New()                                        // Initialize new echo instance
	e.Logger.SetLevel(log.DEBUG)                           // Logger
	e.Pre(middleware.RemoveTrailingSlash())                // Remove any end slash from URI
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{ // CORS with manual config
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization},
		AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete},
	}))
	e.Static("/public", "public")
	e.Use(middleware.Secure())                                             // initialize Secure() middleware, Xss, clickjack, etc
	e.Use(middleware.Logger())                                             // initialize Logger() middleware
	e.Use(middleware.Recover())                                            // initialize Recover() middleware, recover from any panic
	e.Use(session.Middleware(sessions.NewCookieStore([]byte("alHazen3")))) //Use `cookie store` for gorilla/session middleware

	templates := make(map[string]*template.Template) // make new templates key, val maps
	templates["home.html"] = template.Must(template.ParseFiles("public/home.html", "public/index.html"))
	templates["404.html"] = template.Must(template.ParseFiles("public/404.html", "public/index.html"))
	templates["admin.html"] = template.Must(template.ParseFiles("public/admin.html", "public/index.html"))
	templates["admin-signin.html"] = template.Must(template.ParseFiles("public/admin-signin.html", "public/index.html"))
	templates["contact.html"] = template.Must(template.ParseFiles("public/contact.html", "public/index.html"))
	templates["signin.html"] = template.Must(template.ParseFiles("public/signin.html", "public/index.html"))
	templates["successful.html"] = template.Must(template.ParseFiles("public/successful.html", "public/index.html"))
	templates["validate.html"] = template.Must(template.ParseFiles("public/validate.html", "public/index.html"))
	templates["disable.html"] = template.Must(template.ParseFiles("public/disable.html", "public/index.html"))

	// With Template Func
	templates["generate.html"], _ = template.New("public/generate.html").Funcs(template.FuncMap{
		"humanDate": nested.HumanDate}).ParseFiles("public/generate.html", "public/index.html")
	templates["allgenerated.html"], _ = template.New("public/allgenerated.html").Funcs(template.FuncMap{
		"humanDate": nested.HumanDate}).ParseFiles("public/allgenerated.html", "public/index.html")

	e.Renderer = &nested.TemplateRegistry{Templates: templates} // use templates declared above for render

	// This is the routes for public acess
	e.GET("/", handler.HomePage)
	e.GET("/contact", handler.ContactPage)
	e.GET("/signin", handler.SignInPage)
	e.POST("/login", h.UseInvitationID)
	e.GET("successful", handler.SuccessfulPage)
	e.GET("/validate", handler.ValidatePage)
	e.POST("/validate", h.ValidateInvitationID)
	e.GET("/useid", handler.SignInPage)

	// This is the route for Admin access only
	// Put Session middleware
	e.GET("/admin", handler.AdminSigninPage)
	e.POST("/admin", handler.AuthenticateAdminLogin)
	e.GET("/admin/profile", h.AdminProfilePage, utils.AdminSessions)
	e.GET("/admin/allids", h.TabulateAllInvitationIDs, utils.AdminSessions)
	e.POST("/admin/generate", h.AdminGenerateIDs, utils.AdminSessions)
	e.GET("/admin/invitation/:id", h.AdminGetInvitationBasedOnID, utils.AdminSessions)
	e.GET("/admin/disable", handler.DisablePage, utils.AdminSessions)
	e.POST("/admin/disable", h.DisableInvitationBasedOnString, utils.AdminSessions)

	// Use prometheus to check metrics. at `/metrics`
	p := prometheus.NewPrometheus("echo", nil)
	p.Use(e)
	//e.Use(echoPrometheus.MetricsMiddleware())
	//e.GET("/metrics", echo.WrapHandler(promhttp.Handler()))

	//return the echo.Echo instance to main()
	return e
}
