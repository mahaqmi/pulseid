module dev/PulseID

go 1.13

require (
	github.com/globocom/echo-prometheus v0.1.2
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/gorilla/sessions v1.2.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/echo-contrib v0.7.0
	github.com/labstack/echo/v4 v4.1.11
	github.com/labstack/gommon v0.3.0
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/lib/pq v1.2.0
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/mholt/certmagic v0.8.3
	github.com/prometheus/client_golang v1.2.1
	github.com/stretchr/testify v1.4.0
	github.com/valyala/fasttemplate v1.1.0 // indirect
	golang.org/x/crypto v0.0.0-20191206172530-e9b2fee46413
	golang.org/x/net v0.0.0-20191207000613-e7e4b65ae663 // indirect
	golang.org/x/sys v0.0.0-20191206220618-eeba5f6aabab // indirect
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
	gopkg.in/go-playground/validator.v9 v9.30.2
)
