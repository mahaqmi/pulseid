package handler

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
	"time"
)

// Function to allow public to use the Invitation IDs
func (h *Handler) UseInvitationID(c echo.Context) error {
	// Get the invitation string from previous passed form value.
	invString := c.FormValue("invString")
	inv, err := h.InvitationModelImpl.GetInvitationBasedOnString(invString)
	// If there is any problem (eg. the Inv string doesnt exist -> go to `NotFound` page
	if err != nil {
		return c.Render(http.StatusNotFound, "404.html", map[string]interface{}{})
	}
	// If the invitationID is disable, used or expired; show `notfound` page
	if (inv.IsActive == false) || (inv.IsUsed == true) || (inv.TimeExpired.Before(time.Now())) {
		return c.Render(http.StatusSeeOther, "404.html", map[string]interface{}{})
	}
	invID, err := h.InvitationModelImpl.UpdateInvitationUsed(inv.InvitationID)
	if err != nil {
		return err
	}
	fmt.Println(invID)

	// If successfully update the inv ID to disable -> render the `successful` page
	return c.Render(http.StatusOK, "successful.html", map[string]interface{}{
		"invitation": inv,
	})
}

// Function to validate the invitation ID -> generate info about the ID
func (h *Handler) ValidateInvitationID(c echo.Context) error {
	invString := c.FormValue("invString")
	inv, err := h.InvitationModelImpl.GetInvitationBasedOnString(invString)
	// If there is any problem (eg. the Inv string doesnt exist -> go to `NotFound` page
	if err != nil {
		return c.Render(http.StatusNotFound, "404.html", map[string]interface{}{})
	}

	// If the invitation ID exist and no problem, -> render the info of that particular ID
	return c.Render(http.StatusOK, "generate.html", map[string]interface{}{
		"invitation": inv,
	})
}
