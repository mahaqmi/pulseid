package handler

import (
	session2 "github.com/labstack/echo-contrib/session"
	"net/http"

	"github.com/labstack/echo/v4"
)

// Testing for upload (dummy file from go-echo doc)
func UploadForm(c echo.Context) error {
	return c.Render(http.StatusOK, "upload.html", map[string]interface{}{})
}

// Render the `HomePage`; delete any session info (kick out admin session)
func HomePage(c echo.Context) error {
	session, err := session2.Get("session", c)
	if err != nil {
		return err
	}

	if session.Values["LogInType"] == "admin" {
		// Remove session details
		session.Options.MaxAge = -1
		// Add flash stating that the admin user has been logged out
		session.AddFlash("Logged Out from Admin")

		// Save the session removed info
		session.Save(c.Request(), c.Response())
	}
	// If there is flash message; display it at `home` page
	if flashes := session.Flashes(); len(flashes) > 0 {
		err := session.Save(c.Request(), c.Response())
		if err != nil {
			return err
		}
		//Render login page with flashes; If there is flashes to show
		return c.Render(http.StatusOK, "home.html", map[string]interface{}{
			"flashes": flashes,
		})
	}
	return c.Render(http.StatusOK, "home.html", map[string]interface{}{})
}

// Render the `NotFound`
func NotFoundPage(c echo.Context) error {
	return c.Render(http.StatusOK, "404.html", map[string]interface{}{})
}

// Render `Admin Sign In` page
func AdminSigninPage(c echo.Context) error {
	sess, _ := session2.Get("session", c)
	if flashes := sess.Flashes(); len(flashes) > 0 {
		err := sess.Save(c.Request(), c.Response())
		if err != nil {
			return err
		}
		//Render login page with flashes; If there is flashes to show
		return c.Render(http.StatusOK, "admin-signin.html", map[string]interface{}{
			"flashes": flashes,
		})
	}
	return c.Render(http.StatusOK, "admin-signin.html", map[string]interface{}{})
}

// Render `Contact Me` page
func ContactPage(c echo.Context) error {
	return c.Render(http.StatusOK, "contact.html", map[string]interface{}{})
}

// Render page to use the invitation ID
func SignInPage(c echo.Context) error {
	return c.Render(http.StatusOK, "signin.html", map[string]interface{}{})
}

// Render page for successful actions
func SuccessfulPage(c echo.Context) error {
	return c.Render(http.StatusOK, "successful.html", map[string]interface{}{})
}

// Render page for validating invitation ID
func ValidatePage(c echo.Context) error {
	return c.Render(http.StatusOK, "validate.html", map[string]interface{}{})
}

// Render page for admin to disable invitation ID
func DisablePage(c echo.Context) error {
	return c.Render(http.StatusOK, "disable.html", map[string]interface{}{})
}
