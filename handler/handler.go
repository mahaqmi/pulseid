package handler

import (
	"dev/PulseID/pkg/postgres"
)

type Handler struct {
	InvitationModelImpl postgres.InvitationModelImpl
}

// Used in test phase
func NewHandler(i postgres.InvitationModelImpl) *Handler {
	return &Handler{
		InvitationModelImpl: i,
	}
}
