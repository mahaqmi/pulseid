package handler

import (
	"fmt"
	session2 "github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

// Function to authenticate the admin Login. This uses a simple auth method
func AuthenticateAdminLogin(c echo.Context) error {
	// Get the value of `user` and `password` from previous form
	user := c.FormValue("user")
	password := c.FormValue("password")

	fmt.Printf("user:%s, password:%s", user, password)
	// Initialize session. use name `session` for the session
	sess, _ := session2.Get("session", c)
	// If the user is `admin` and password is `qwerty123` put the value of `admin` into the map `logintype`
	if (user == "admin") && (password == "qwerty123") {
		sess.Values["LogInType"] = "admin"
		sess.Save(c.Request(), c.Response())
	} else {
		// If the user input wrongly, create a flash message + redirect back to the same page
		sess.AddFlash("Invalid credentials")
		sess.Save(c.Request(), c.Response())
		return c.Redirect(http.StatusSeeOther, "/admin")
	}
	// Redirect to admin dashboard/profile if success
	return c.Redirect(http.StatusSeeOther, "/admin/profile")
}

// Function to render admin dashboard/profile page with the summary info of Invitation IDs
func (h *Handler) AdminProfilePage(c echo.Context) error {
	total, unused, used, disable, err := h.InvitationModelImpl.GetAllInDepthInvitations()
	if err != nil {
		return err
	}

	sess, _ := session2.Get("generate" ,c)
	// If there is flash message; display it at `home` page
	if flashes := sess.Flashes(); len(flashes) > 0 {
		err := sess.Save(c.Request(), c.Response())
		if err != nil {
			return err
		}
		//Render login page with flashes; If there is flashes to show
		return c.Render(http.StatusOK, "admin.html", map[string]interface{}{
			"flashes": flashes,
			"total":   total,
			"unused":  unused,
			"used":    used,
			"disable": disable,
		})
	}
	return c.Render(http.StatusOK, "admin.html", map[string]interface{}{
		"total":   total,
		"unused":  unused,
		"used":    used,
		"disable": disable,
	})
}

// Function to let admin generate new Invitation ID
func (h *Handler) AdminGenerateIDs(c echo.Context) error {
	length, err := strconv.Atoi(c.FormValue("length"))
	if (length<6)||(length>12){
		sess, _ := session2.Get("generate", c)
		// If the user input wrongly, create a flash message + redirect back to the same page
		sess.AddFlash("Please input value between 6 to 12")
		sess.Save(c.Request(), c.Response())
		return c.Redirect(http.StatusSeeOther, "/admin/profile")
	}
	id, err := h.InvitationModelImpl.GenerateInvitation(length)
	if err != nil {
		return err
	}

	return c.Redirect(http.StatusSeeOther, fmt.Sprintf("/admin/invitation/%d", id))
}

// Function to get Invitation ID info from its id
func (h *Handler) AdminGetInvitationBasedOnID(c echo.Context) error {
	// Get the id from URI param, convert it to interface -> Atoi(ASCII to int)
	id, _ := strconv.Atoi(c.Param("id"))

	inv, err := h.InvitationModelImpl.GetInvitationBasedOnID(id)
	if err != nil {
		return err
	}

	return c.Render(http.StatusOK, "generate.html", map[string]interface{}{
		"invitation": inv,
	})
}

// Function to tabulate all existing invitation IDs
func (h *Handler) TabulateAllInvitationIDs(c echo.Context) error {
	invitations, err := h.InvitationModelImpl.GetAllInvitation()
	if err != nil {
		return err
	}

	return c.Render(http.StatusOK, "allgenerated.html", map[string]interface{}{
		"invitations": invitations,
	})
}

// Function to allow admin to disable any `active` invitation IDs
func (h *Handler) DisableInvitationBasedOnString(c echo.Context) error {
	inviteID, err := h.InvitationModelImpl.GetInvitationBasedOnString(c.FormValue("invString"))
	if err != nil {
		return c.Render(http.StatusNotFound, "404.html", map[string]interface{}{})
	}
	invID, err := h.InvitationModelImpl.UpdateInvitationActive(inviteID.InvitationID)
	if err != nil {
		return err
	}

	return c.Redirect(http.StatusSeeOther, fmt.Sprintf("/admin/invitation/%d", invID))
}
