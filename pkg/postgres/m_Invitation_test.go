package postgres

import (
	"dev/PulseID/test"
	"github.com/stretchr/testify/assert"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"
)

// Test func `GenerateInvitation`
func TestGenerateInvitation(t *testing.T) {
	mockDB, mock, sqlxDB := test.MockDB(t)
	defer mockDB.Close()

	var cols []string = []string{"InvitationID", "InvitationString", "TimeCreated", "TimeExpired", "IsUsed", "IsActive"}
	mock.ExpectQuery("SELECT *").WillReturnRows(sqlmock.NewRows(cols).
		AddRow(0, "nwPuuWgwhLqd", time.Time{}, time.Time{}, false, false))

	im := NewInvitationModel(sqlxDB)
	u, _ := im.GenerateInvitation()

	expect := 1
	assert.Equal(t, expect, u)
}

// Test func `GetInvitationBasedOnID`
func TestGetInvitationBasedOnID(t *testing.T) {
	mockDB, mock, sqlxDB := test.MockDB(t)
	defer mockDB.Close()

	var cols []string = []string{"InvitationID", "InvitationString", "TimeCreated", "TimeExpired", "IsUsed", "IsActive"}
	mock.ExpectQuery("SELECT *").WillReturnRows(sqlmock.NewRows(cols).
		AddRow(1, "nwPuuWgwhLqd", time.Time{}, time.Time{}, false, false))

	im := NewInvitationModel(sqlxDB)
	u, _ := im.GetInvitationBasedOnID(1)

	expect := Invitation{
		InvitationID:     1,
		InvitationString: "nwPuuWgwhLqd",
		TimeCreated:      time.Time{},
		TimeExpired:      time.Time{},
		IsUsed:           false,
		IsActive:         false,
	}
	assert.Equal(t, expect, u)
}

// Test func GetInvitationBasedOnString
func TestGetInvitationBasedOnString(t *testing.T) {
	mockDB, mock, sqlxDB := test.MockDB(t)
	defer mockDB.Close()

	var cols []string = []string{"InvitationID", "InvitationString", "TimeCreated", "TimeExpired", "IsUsed", "IsActive"}
	mock.ExpectQuery("SELECT *").WillReturnRows(sqlmock.NewRows(cols).
		AddRow(1, "nwPuuWgwhLqd", time.Time{}, time.Time{}, false, false))

	im := NewInvitationModel(sqlxDB)
	u, _ := im.GetInvitationBasedOnString("nwPuuWgwhLqd")

	expect := Invitation{
		InvitationID:     1,
		InvitationString: "nwPuuWgwhLqd",
		TimeCreated:      time.Time{},
		TimeExpired:      time.Time{},
		IsUsed:           false,
		IsActive:         false,
	}
	assert.Equal(t, expect, u)
}

// Test func GetAllInvitation
func TestGetAllInvitation(t *testing.T) {
	mockDB, mock, sqlxDB := test.MockDB(t)
	defer mockDB.Close()

	u1 := Invitation{
		InvitationID:     1,
		InvitationString: "nwPuuWgwhLqd",
		TimeCreated:      time.Time{},
		TimeExpired:      time.Time{},
		IsUsed:           false,
		IsActive:         false,
	}
	u2 := Invitation{
		InvitationID:     2,
		InvitationString: "ltWk7NFLYcXA",
		TimeCreated:      time.Time{},
		TimeExpired:      time.Time{},
		IsUsed:           false,
		IsActive:         false,
	}
	u3 := Invitation{
		InvitationID:     3,
		InvitationString: "lAhA7esHNmyq",
		TimeCreated:      time.Time{},
		TimeExpired:      time.Time{},
		IsUsed:           false,
		IsActive:         false,
	}

	var cols []string = []string{"InvitationID", "InvitationString", "TimeCreated", "TimeExpired", "IsUsed", "IsActive"}
	mock.ExpectQuery("SELECT *").WillReturnRows(sqlmock.NewRows(cols).
		AddRow(u1.InvitationID, u1.InvitationString, u1.TimeCreated, u1.TimeExpired, u1.IsUsed, u1.IsActive).
		AddRow(u2.InvitationID, u2.InvitationString, u2.TimeCreated, u2.TimeExpired, u2.IsUsed, u2.IsActive).
		AddRow(u3.InvitationID, u3.InvitationString, u3.TimeCreated, u3.TimeExpired, u3.IsUsed, u3.IsActive))

	im := NewInvitationModel(sqlxDB)
	u, _ := im.GetAllInvitation()

	expect := []Invitation{}
	expect = append(expect, u1, u2, u3)
	assert.Equal(t, expect, u)
}
