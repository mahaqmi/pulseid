package postgres

import (
	"dev/PulseID/utils"
	"github.com/jmoiron/sqlx"
	"time"
)

type (
	// Declare the interface for these methods
	InvitationModelImpl interface {
		GenerateInvitation(int) (int, error)
		UpdateInvitationUsed(invitationID int) (int, error)
		UpdateInvitationActive(invitationID int) (int, error)
		GetInvitationBasedOnID(id int) (Invitation, error)
		GetInvitationBasedOnString(invString string) (Invitation, error)
		GetAllInvitation() ([]Invitation, error)
		GetAllInDepthInvitations() (int, int, int, int, error)
	}

	// Define struct for `Invitation` ( to be use for DB table)
	Invitation struct {
		InvitationID     int       `json:"invitation_InvitationID" db:"InvitationID"`
		InvitationString string    `json:"invitation_InvitationString" db:"InvitationString"`
		TimeCreated      time.Time `json:"invitation_TimeCreated" db:"TimeCreated"`
		TimeExpired      time.Time `json:"invitation_TimeExpired" db:"TimeExpired"`
		IsUsed           bool      `json:"invitation_IsUsed" db:"IsUsed"`
		IsActive         bool      `json:"invitation_IsActive"db:"IsActive"`
	}

	// Define struct for Model that points to sqlx DB, to allow use all its methods
	InvitationModel struct {
		db *sqlx.DB
	}
)

// Use in test phase
func NewInvitationModel(db *sqlx.DB) *InvitationModel {
	return &InvitationModel{
		db: db,
	}
}

// Function to generate invitation ID
func (i *InvitationModel) GenerateInvitation(length int) (int, error) {
	var (
		InvitationID int
	)
	invitationString := utils.String(length)
	row := i.db.QueryRow(`INSERT INTO "Invitation" ("InvitationString", "TimeCreated", "TimeExpired", "IsUsed", "IsActive") 
VALUES($1,$2,$3,$4,$5) RETURNING "InvitationID"`, invitationString, time.Now(), time.Now().Local().Add(time.Hour*time.Duration(168)), false, true)

	err := row.Scan(&InvitationID)

	if err != nil {
		return 0, err
	}

	// The ID returned has the type int64, so we convert it to an int type before returning.
	return int(InvitationID), nil
}

// Function to Update invitation ID form `notUsed` to `Used`
func (i *InvitationModel) UpdateInvitationUsed(invitationID int) (int, error) {
	invitation := Invitation{}
	var InvID int

	err := i.db.QueryRowx(`UPDATE "Invitation" SET "IsUsed"=true WHERE "InvitationID"=$1 RETURNING "InvitationID"`, invitationID).StructScan(&invitation)

	if err != nil {
		// return 0 for int (to avoid use panic(err))
		return 0, err
	}
	return int(InvID), nil
}

// Function to Update invitation ID status from `active` to `disable`
func (i *InvitationModel) UpdateInvitationActive(invitationID int) (int, error) {
	invitation := Invitation{}

	err := i.db.QueryRowx(`UPDATE "Invitation" SET "IsActive"=false WHERE "InvitationID"=$1 RETURNING "InvitationID"`, invitationID).StructScan(&invitation)
	if err != nil {
		// return 0 for int (to avoid use panic(err))
		return 0, err
	}

	return int(invitation.InvitationID), nil
}

// Function to get invitation ID info based on its ID
func (i *InvitationModel) GetInvitationBasedOnID(id int) (Invitation, error) {
	invitation := Invitation{}

	err := i.db.Get(&invitation, `SELECT * FROM "Invitation" WHERE "InvitationID" = $1`, id)
	if err != nil {
		return invitation, err
	}

	return invitation, nil
}

// Function to get invitation ID info based on its String
func (i *InvitationModel) GetInvitationBasedOnString(invString string) (Invitation, error) {
	invitation := Invitation{}

	err := i.db.Get(&invitation, `SELECT * FROM "Invitation" WHERE "InvitationString" = $1`, invString)
	if err != nil {
		return invitation, err
	}

	return invitation, nil
}

// Function to get all invitation IDs from `Invitation` table
func (i *InvitationModel) GetAllInvitation() ([]Invitation, error) {
	invitations := []Invitation{}

	err := i.db.Select(&invitations, `SELECT * FROM "Invitation"`)
	if err != nil {
		return invitations, err
	}

	return invitations, nil
}

// Function to get info for amount `total`, `used`, `unused`, `disable`. (To be used for admin dashboard/profile)
func (i *InvitationModel) GetAllInDepthInvitations() (int, int, int, int, error) {
	invitations := []*Invitation{}

	err := i.db.Select(&invitations, `SELECT * FROM "Invitation"`)
	if err != nil {
		return 0, 0, 0, 0, err
	}
	//get details
	total := 0
	unused := 0
	used := 0
	disable := 0
	for _, v := range invitations {
		total += 1
		if v.IsUsed == true {
			used += 1
		}
		if v.IsUsed == false {
			unused += 1
		}
		if v.IsActive == false {
			disable += 1
		}
	}

	return total, unused, used, disable, nil
}
