package utils

import (
	"math/rand"
	"time"
)

// From jonCalhoun blog

// Character set to be used for the string generator
const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

// Pseudorandom generator
var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

// Generate string with particular length and character set
func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// Function that abstract the character set (use the default) and only set the length
func String(length int) string {
	return StringWithCharset(length, charset)
}
