package utils

import (
	session2 "github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"net/http"
)

// Middleware function for admin session; if the user doesn't have this session -> kick him and redirect to `homePage`
func AdminSessions(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		sess, err := session2.Get("session", c)
		if err != nil {
			return err
		}
		LogInType := sess.Values["LogInType"]
		if LogInType != "admin" {
			sess.AddFlash("Please log in as a admin!", "message")
			return c.Redirect(http.StatusSeeOther, "/")
		}
		return next(c)
	}
}
